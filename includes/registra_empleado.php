<?php
session_start();
if (isset($_SESSION['id_usuario'])) {
    include_once 'conexion.php';
    $conexion = new Conexion();

    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        if (!isset($_POST['primer_nombre'])||!isset($_POST['primer_apellido'])||!isset($_POST['genero'])||!isset($_POST['celular'])||empty($_POST['primer_nombre'])||empty($_POST['primer_apellido'])||empty($_POST['genero'])||empty($_POST['celular'])) {
            $error = "Los campos, <b>Primer Nombre, Primer Apellido, Genero y Nro. Celular</b> son obligatorios";
            setcookie("error", $error, time() + 10, '/');
            header('Location: ' . getenv('HTTP_REFERER'));

        }
        $primer_nombre = htmlentities(strip_tags(trim($_POST['primer_nombre'])));
        $segundo_nombre = htmlentities(strip_tags(trim($_POST['segundo_nombre'])));
        $primer_apellido = htmlentities(strip_tags(trim($_POST['primer_apellido'])));
        $segundo_apellido = htmlentities(strip_tags(trim($_POST['segundo_apellido'])));
        $genero = trim($_POST['genero']);
        $celular = htmlentities(strip_tags(trim($_POST['celular'])));
        $direccion = htmlentities(strip_tags(trim($_POST['direccion'])));
        $fotografia = NULL;

        if (isset($_FILES['fotografia'])) {
            $tipo = $_FILES['fotografia']['type'];
            if ($_FILES['fotografia']['error']==0) {
                $nombre_fotografia = $_FILES['fotografia']['name'];
                $extension = pathinfo($nombre_fotografia, PATHINFO_EXTENSION);
                if ($extension=='jpg'||$extension=='jpeg'||$extension=='png') {
                    $ruta_temporal = $_FILES['fotografia']['tmp_name'];
                    $nuevo_nombre =  "empleado_". bin2hex(random_bytes(8)) . date('Ymd_His') . '.'.$extension;
                    $ruta_destino = "../fotografia-empleado/normal/$nuevo_nombre";
                    if (move_uploaded_file($ruta_temporal, $ruta_destino)){
                        $fotografia = $nuevo_nombre;
                    }else{
                        $error = "No se pudo subir la fotografia, intente nuevamente por favor";
                    }
                }else{
                    $error = "El tipo de archivo no es permitido, por favor seleccione una imagen JPG o PNG";
                }
            }
        }
        if (isset($error)) {
            setcookie("error", $error, time() + 10, '/');
            header('Location: ' . getenv('HTTP_REFERER'));
            exit();
        }
        $query = $conexion->connect()->prepare('INSERT INTO empleados(primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, celular, direccion, fotografia) VALUES(:primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :genero, :celular, :direccion, :fotografia)');
        $query->execute(
            [
                ':primer_nombre' => $primer_nombre,
                ':segundo_nombre' => $segundo_nombre,
                ':primer_apellido' => $primer_apellido,
                ':segundo_apellido' => $segundo_apellido,
                ':genero' => $genero,
                ':celular' => $celular,
                ':direccion' => $direccion,
                ':fotografia' => $fotografia,
            ]
        );
        $mensaje = "Se registró al empleado correctamente";
        setcookie("confirmado", $mensaje, time() + 10, '/');

        header('location: ../vistas/listado_empleados.php');
    }else{
        header('location: ../index.php');
    }
}else{
  header('location: ../index.php');
}

?>