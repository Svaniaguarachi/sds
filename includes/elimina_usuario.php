<?php
session_start();
if (isset($_SESSION['id_usuario'])) {
	include_once 'user.php';
	include_once 'conexion.php';

	$conexion = new Conexion();
	$user = new User();


	$usuario = $user->getUsuario($_SESSION['id_usuario']);

	if (strtolower($usuario['rol']) == 'admin') {
		if (isset($_GET['usuario'])||!empty($_GET['usuario'])) {
			$id = base64_decode(base64_decode(base64_decode(base64_decode($_GET['usuario']))));

			$query = $conexion->connect()->prepare('DELETE FROM usuarios WHERE id = :id');
			$query->execute(array(':id' => $id));

			if ($_SESSION['id_usuario']==$id) {
				session_unset();
	        	session_destroy();
			}

			$mensaje = "Se eliminó al usuario correctamente";
			setcookie("confirmado", $mensaje, time() + 10, '/');
			header('location: ../vistas/listado_usuarios.php');
		}else{
			header('location: ../vistas/listado_usuarios.php');
		}
	}else{
		header('location: ../index.php');
	}
}else{
	header('location: ../index.php');
}

?>