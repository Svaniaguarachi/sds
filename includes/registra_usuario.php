<?php
session_start();
if (isset($_SESSION['id_usuario'])) {
    include_once 'conexion.php';
    $conexion = new Conexion();

    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        if (!isset($_POST['primer_nombre'])||!isset($_POST['primer_apellido'])||!isset($_POST['rol'])||!isset($_POST['password'])||empty($_POST['primer_nombre'])||empty($_POST['primer_apellido'])||empty($_POST['rol'])||empty($_POST['password'])) {
            $error = "Los campos, <b>Primer Nombre, Primer Apellido, Rol y Password</b> son obligatorios";
            setcookie("error", $error, time() + 10, '/');
            header('Location: ' . getenv('HTTP_REFERER'));

        }
        $primer_nombre = htmlentities(strip_tags(trim($_POST['primer_nombre'])));
        $segundo_nombre = htmlentities(strip_tags(trim($_POST['segundo_nombre'])));
        $primer_apellido = htmlentities(strip_tags(trim($_POST['primer_apellido'])));
        $segundo_apellido = htmlentities(strip_tags(trim($_POST['segundo_apellido'])));
        $rol = trim($_POST['rol']);
        $password = htmlentities(strip_tags(trim($_POST['password'])));

        $expresion = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_;,])([A-Za-z\d$@$!%*?&_;,]|[^ ]){8,15}$/";

        if(preg_match($expresion, $password)){
            $usuario = ucwords(substr($primer_nombre, 0, 1)) . strtolower($primer_apellido);
            $query = $conexion->connect()->prepare('SELECT * FROM usuarios WHERE usuario = :usuario');
            $query->execute(array('usuario' => $usuario));
            $nuevo_usuario = $query->fetch();

            if (isset($nuevo_usuario['usuario'])) {
                $usuario .= substr($segundo_apellido, 0, 2);

                $query = $conexion->connect()->prepare('SELECT * FROM usuarios WHERE usuario = :usuario');
                $query->execute(array('usuario' => $usuario));
                $nuevo_usuario = $query->fetch();
                if (isset($nuevo_usuario['usuario'])){
                    $usuario .= substr($segundo_apellido, 0, 2).date('His');
                }
            }

            $query = $conexion->connect()->prepare('INSERT INTO usuarios(primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, rol, usuario, clave) VALUES(:primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :rol, :usuario, :clave)');
            $query->execute(
                [
                    ':primer_nombre' => $primer_nombre,
                    ':segundo_nombre' => $segundo_nombre,
                    ':primer_apellido' => $primer_apellido,
                    ':segundo_apellido' => $segundo_apellido,
                    ':rol' => $rol,
                    ':usuario' => $usuario,
                    ':clave' => password_hash($password, PASSWORD_DEFAULT),
                ]
            );
            $mensaje = "<b>Se registró al usuario correctamente</b>";
            setcookie("confirmado", $mensaje, time() + 10, '/');

            header('location: ../vistas/listado_usuarios.php');
        }else{
            $error = "<b>El password debee tener un Mínimo de 8 caracteres, debe contener al menos una letra mayúscula y minúscula, un número y un carácter especial </b>";
            setcookie("error", $error, time() + 10, '/');
            header('Location: ' . getenv('HTTP_REFERER'));
        }
    }else{
        header('location: ../index.php');
    }
}else{
  header('location: ../index.php');
}

?>
