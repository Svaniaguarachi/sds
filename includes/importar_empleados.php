<?php
session_start();
if (isset($_SESSION['id_usuario'])) {
    include_once 'user.php';
    
    $conexion = new Conexion();
    $user = new User();

    $usuario = $user->getUsuario($_SESSION['id_usuario']);
    $token = "56614b822e09eeb7c29b9fe841e79e5cf78329378ef7f915cca45cb678c4bf3660b45fce144a8405c0495e2e059e31a288818b799a6b77c0562d2d8c7a0d89714b5c4d9a65585000b8e8f15c4441c424fedf142f5937fb8b81550bd39e9463235d16ad3e6c3315648ab4652aa51bb2f0d96a335d22cc0c392c450405ca04486e";
    if (strtolower($usuario['rol']) == 'admin') {

        $datos = file_get_contents("importar_empleados.json");
        $empleados = json_decode($datos, true);
        $cont = 0;
        foreach ($empleados as $empleado) {
            if (strtolower($empleado['gender'])=='Female') {
                $genero = 'femenino';
            }else{
                $genero = 'masculino';
            }
            $query = $conexion->connect()->prepare('INSERT INTO empleados(primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, celular, direccion, fotografia) VALUES(:primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :genero, :celular, :direccion, :fotografia)');
            $query->execute(
                [
                    ':primer_nombre' => $empleado['first_name'],
                    ':segundo_nombre' => $empleado['first_name2'],
                    ':primer_apellido' => $empleado['last_name'],
                    ':segundo_apellido' => $empleado['last_name2'],
                    ':genero' => $genero,
                    ':celular' => '7'.rand(1000000, 9999999),
                    ':direccion' => $empleado['address'],
                    ':fotografia' => NULL,
                ]
            );
            $cont++;
        }

        $mensaje = "Se registró $cont empleados, correctamente";
        setcookie("confirmado", $mensaje, time() + 15, '/');

        header('location: ../vistas/listado_empleados.php');
    }else{
        header('location: ../index.php');
    }
}else{
  header('location: ../index.php');
}

?>