<?php
session_start();
include_once '../includes/user.php';

if (!isset($_SESSION['id_usuario'])) {
  header('location: ../index.php');
  exit();
}

$conexion = new Conexion();
$user = new User();

$usuario = $user->getUsuario($_SESSION['id_usuario']);

if (strtolower($usuario['rol']) != 'admin') {
  header('location: ../index.php');
  exit();
}

$sql = 'SELECT * FROM usuarios ORDER BY primer_nombre';
$query = $conexion->connect()->prepare($sql);
$query->execute();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="../assets/img/icono.ico" type="image/x-icon">
  <link rel="apple-touch-icon" href="../assets/img/icono.ico">
  <link rel="stylesheet" href="../assets/bootstrap-4.6/css/bootstrap.css">
  <title>Listado de usuarios</title>
</head>
<body>
  <?php include_once 'navbar.php'; ?>
  <div class="container mt-3">
    <div class="row">
      <div class="col-12">

          <?php if (isset($error)) { ?>
            <div class="alert alert-danger alert-dismissible text-center fade show" role="alert"><?php echo $error; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } ?>
          <?php if (isset($_COOKIE['confirmado'])) { ?>
            <div class="alert alert-success alert-dismissible text-center fade show" role="alert"><?php echo $_COOKIE['confirmado']; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } ?>

      </div>
      <div class="col-12 col-lg-10 mx-auto">            
      	<h4 class="text-dark text-center my-3">Listado de usuarios</h4>
      	<div class="input-group is-invalid pb-3">
      		<a href="registrar_usuario.php" class="btn btn-primary">Registrar Usuario</a>
      		<input type="hidden" class="hidden-print hidden" name="token" hidden readonly value="<?php echo bin2hex(random_bytes(128)) ?>">
      	</div>
      </div>
      <div class="col-12 col-lg-10 mx-auto">
        <table class="table table-bordered table-responsive-md table-striped table-hover w-100">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Usuario</th>
              <th scope="col">Nombres</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Rol</th>
              <th scope="col">Editar</th>
              <th scope="col">Eliminar</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($query as $i=>$usuario) { ?>
              <tr>
                <td><?php echo ($i+1) ?></td>
                <td><?php echo $usuario['usuario'] ?></td>
                <td><?php echo $usuario['primer_nombre'].' '.$usuario['segundo_nombre'] ?></td>
                <td><?php echo $usuario['primer_apellido'].' '.$usuario['segundo_apellido'] ?></td>
                <td><?php echo ucfirst($usuario['rol']) ?></td>
                <td>
                  <a href="editar_usuario.php?usuario=<?php echo (base64_encode(base64_encode(base64_encode(base64_encode($usuario['id']))))); ?>" class="btn btn-outline-primary">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" class="svg-inline--fa fa-pencil-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="20"><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg>
                  </a>
                </td>
                <td>
                  <a href="../includes/elimina_usuario.php?usuario=<?php echo (base64_encode(base64_encode(base64_encode(base64_encode($usuario['id']))))); ?>" onclick="return confirm('¿Está seguro de eliminar el usuario?')" class="btn btn-outline-danger">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path></svg>
                  </a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="../assets/jquery-3.6.0.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-4.6/js/bootstrap.js"></script>
  <script type="text/javascript">
    function limpiarFormulario() {
      $("#form-buscar").find('.form-control').val('');
    }
  </script>
</body>
</html>
