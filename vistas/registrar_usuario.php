<?php 
session_start();
include_once '../includes/user.php';
include_once '../includes/conexion.php';

if (!isset($_SESSION['id_usuario'])) {
  header('location: ../index.php');
  exit();
}
$conexion = new Conexion();
$user = new User();

$usuario = $user->getUsuario($_SESSION['id_usuario']);
if (strtolower($usuario['rol']) != 'admin') {
  header('location: ../index.php');
  exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="../assets/img/icono.ico" type="image/x-icon">
  <link rel="apple-touch-icon" href="../assets/img/icono.ico">
  <link rel="stylesheet" href="../assets/bootstrap-4.6/css/bootstrap.css">
  <title>Registrar usuario</title>
</head>
<body>
  <?php include_once 'navbar.php'; ?>

  <div class="container mt-3 mt-lg-5">
    <div class="row justify-content-center">
      <div class="col-12">
        <?php if (isset($_COOKIE["error"])) { ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert"><?php echo $_COOKIE["error"]; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <?php } ?>
      <h3 class="text-center">Formulario de registro</h3>
      </div>
      <div class="col-12">
        <form method="POST" action="../includes/registra_usuario.php" accept-charset="UTF-8" name="registrar_usuario" id="registrar_usuario" role="form" class="was-validated form-horizontal prevent-double-submit" autocomplete="off" enctype="multipart/form-data">

          <div class="row">
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="primer_nombre">Primer Nombre</label>
                <input type="text" class="form-control is-invalid" name="primer_nombre" id="primer_nombre" value="" required placeholder="Ej: Swania">
              </div>          
            </div>
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="segundo_nombre">Segundo Nombre</label>
                <input type="text" class="form-control is-invalid" name="segundo_nombre" id="segundo_nombre" placeholder="Ej: Bett">
              </div>          
            </div>
            
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="primer_apellido">Primer Apellido</label>
                <input type="text" class="form-control is-invalid" name="primer_apellido" id="primer_apellido" required placeholder="Ej: Guarachi">
              </div>          
            </div> 

            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="segundo_apellido">Segundo Apellido</label>
                <input type="text" class="form-control is-invalid" name="segundo_apellido" id="segundo_apellido" placeholder="Ej: Velasco">
              </div>          
            </div>          
            <div class="col-12 col-md-6 mb-3">
              <label class="custom-control" for="rol">Rol</label>
              <select class="form-control" name="rol" id="rol" required>
                <option value="" selected>Seleccione</option>
                <option value="admin">Admin</option>
                <option value="invitado">Invitado</option>
              </select>
            </div>          
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="password">Password</label>
                <input type="password" class="form-control is-invalid" name="password" id="password" required placeholder="**********">
              </div>          
            </div>          

            <div class="col-12">
              <div class="custom-control p-0 mb-3">
                <button class="btn btn-success" id="button-enviar">Registrar usuario</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-12 mt-5">
        <p class="text-center text-dark font-weight-bold"><small>Copyright © 2021, Swania Guarachi Velasco</small></p>
      </div>
    </div>
    
  </div>
  <script type="text/javascript" src="../assets/jquery-3.6.0.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-4.6/js/bootstrap.js"></script>
</body>
</html>
