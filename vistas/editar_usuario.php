<?php 
session_start();
include_once '../includes/user.php';
include_once '../includes/conexion.php';

if (!isset($_SESSION['id_usuario'])) {
  header('location: ../index.php');
  exit();
}
$conexion = new Conexion();
$user = new User();

if (!isset($_GET['usuario'])) {
  header('location: listado_usuarios.php');
  exit();
}

$usuario = $user->getUsuario($_SESSION['id_usuario']);
if (strtolower($usuario['rol']) != 'admin') {
  header('location: ../index.php');
  exit();
}

$id = base64_decode(base64_decode(base64_decode(base64_decode($_GET['usuario']))));

$query = $conexion->connect()->prepare('SELECT * FROM usuarios WHERE id = :id');
$query->execute(array('id' => $id));
$editar = $query->fetch(); /*Devuelve una fila*/

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="../assets/img/icono.ico" type="image/x-icon">
  <link rel="apple-touch-icon" href="../assets/img/icono.ico">
  <link rel="stylesheet" href="../assets/bootstrap-4.6/css/bootstrap.css">
  <style type="text/css">
    .custom-file-label::after{content: 'Fotografia' !important;}
  </style>
  <title>Editar usuario</title>
</head>
<body>
  <?php include_once 'navbar.php'; ?>

  <div class="container mt-3 mt-lg-5">
    <div class="row justify-content-center">
      <div class="col-12">

          <?php if (isset($_COOKIE['error'])) { ?>
            <div class="alert alert-danger alert-dismissible text-center fade show" role="alert"><?php echo $_COOKIE['error']; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } ?>
          <?php if (isset($_COOKIE['confirmado'])) { ?>
            <div class="alert alert-success alert-dismissible text-center fade show" role="alert"><?php echo $_COOKIE['confirmado']; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } ?>
        <h3 class="text-center">Editar usuario</h3>
      </div>
      <div class="col-12">
        <form class="was-validated mx-auto p-2 p-md-3 p-lg-4 border border-white shadow" action="../includes/edita_usuario.php" method="POST" accept-charset="UTF-8" name="editar_usuario" id="editar_usuario" role="form" autocomplete="off" enctype="multipart/form-data">
          <div class="row">
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="primer_nombre">Primer Nombre</label>
                <input type="text" class="form-control is-invalid" name="primer_nombre" id="primer_nombre" value="<?php echo $editar['primer_nombre'] ?>" required placeholder="Ej: Swania">
              </div>          
            </div>
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="segundo_nombre">Segundo Nombre</label>
                <input type="text" class="form-control is-invalid" name="segundo_nombre" id="segundo_nombre" value="<?php echo $editar['segundo_nombre'] ?>" placeholder="Ej: Betty">
              </div>          
            </div>
            
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="primer_apellido">Primer Apellido</label>
                <input type="text" class="form-control is-invalid" name="primer_apellido" id="primer_apellido" value="<?php echo $editar['primer_apellido'] ?>" required placeholder="Ej: Guarachi">
              </div>          
            </div> 

            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="segundo_apellido">Segundo Apellido</label>
                <input type="text" class="form-control is-invalid" name="segundo_apellido" id="segundo_apellido" value="<?php echo $editar['segundo_apellido'] ?>" placeholder="Ej: Velasco">
              </div>          
            </div>          
            <div class="col-12 col-md-6 mb-3">
              <label class="custom-control" for="rol">Rol</label>
              <select class="form-control" name="rol" id="rol" required>
                <option value="">Seleccione</option>
                <option value="admin" <?php if ($editar['rol']=='admin'){ echo "selected"; } ?>>Admin</option>
                <option value="invitado" <?php if ($editar['rol']=='invitado'){ echo "selected"; } ?>>Invitado</option>
              </select>
            </div>          
        
            <div class="col-12 col-md-6 mb-3">
              <div class="custom-control p-0 mb-3">
                <label class="custom-control" for="usuario">Usuario</label>
                <input type="text" class="form-control is-invalid" required name="usuario" id="usuario" value="<?php echo $editar['usuario'] ?>" placeholder="Ej: Achumani, Calle 17">
              </div>     
            </div>     
   
            <div class="col-12">
              <div class="custom-control p-0 mb-3">
                <input type="hidden" hidden readonly value="<?php echo base64_encode(base64_encode(base64_encode(base64_encode($editar['id'])))); ?>" name="id_usuario" id="id_usuario" class="hidden text-hide">
                <button class="btn btn-success" id="button-enviar">Actualizar usuario</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-12 mt-5">
        <p class="text-center text-dark font-weight-bold"><small>Copyright © 2021, Swania Guarachi Velasco</small></p>
      </div>
    </div>
    
  </div>
  <script type="text/javascript" src="../assets/jquery-3.6.0.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-4.6/js/bootstrap.js"></script>
</body>
</html>