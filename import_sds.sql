-- phpMyAdmin SQL Dump
-- version 4.9.6
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-04-2021 a las 16:12:35
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sds`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `primer_nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segundo_nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primer_apellido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segundo_apellido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genero` enum('masculino','femenino') COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fotografia` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `genero`, `celular`, `direccion`, `fotografia`) VALUES
(255, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(256, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(257, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(258, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(259, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(260, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(261, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(262, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(263, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(264, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(265, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(266, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(267, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(268, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(269, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(270, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(271, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(272, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(273, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(274, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(275, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(276, 'Alejandra', NULL, 'Cespedes', '\0\0\0M\0\0\0o\0\0\0r\0\0\0a\0\0\0l\0\0\0e\0\0\0s', 'femenino', '78965412', 'Sopocachi, Calle. Ricardo Mujia, Nro. 458', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(277, 'Mariana', NULL, 'Delgadillo', '\0\0\0A\0\0\0r\0\0\0i\0\0\0a\0\0\0s', 'femenino', '78963020', 'Achumani, Calle 17, Nro 258', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(278, 'Jorge', 'Federico', 'Mercado', '\0\0\0R\0\0\0o\0\0\0m\0\0\0a\0\0\0n', 'masculino', '79553010', 'Miraflores, C.10', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(283, 'Swania', 'Betty', 'Guarachi', '', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(284, 'Esteban', '', 'Silva', '\0\0\0M\0\0\0e\0\0\0j\0\0\0i\0\0\0a', 'masculino', '75852120', 'Calacoto, Calle 23', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(285, 'Maria', 'Fernanda', 'Alvarez', '\0\0\0C\0\0\0a\0\0\0r\0\0\0i', 'femenino', '68014157', 'Sopocachi', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(286, 'Maria', 'Fernanda', 'Alvarez', '\0\0\0C\0\0\0a\0\0\0r\0\0\0i', 'femenino', '68014157', 'Sopocachi', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(287, 'Maria', 'Fernanda', 'Alvarez', '\0\0\0C\0\0\0a\0\0\0r\0\0\0i', 'femenino', '68014157', 'Sopocachi', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(288, 'Maria', 'Fernanda', 'Alvarez', '\0\0\0C\0\0\0a\0\0\0r\0\0\0i', 'femenino', '68014157', 'Sopocachi', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(289, 'Freddy', '', 'Alvarez', '\0\0\0C\0\0\0a\0\0\0r\0\0\0i', 'masculino', '77514785', 'Guanay', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(290, 'Freddy', '', 'Alvarez', '\0\0\0C\0\0\0a\0\0\0r\0\0\0i', 'masculino', '77514785', 'Guanay', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(291, 'Swania', '', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(292, 'Swania', '', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(293, 'Swania', '', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(294, 'Swania', '', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(295, 'Swania', '', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(296, 'Swania', '', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(297, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(298, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(299, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(300, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(301, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Santiago II, calle 5', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(302, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Sopocachi', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(303, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Sopocachi', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(304, 'Swania', 'Betty', 'Guarachi', '\0\0\0V\0\0\0e\0\0\0l\0\0\0a\0\0\0s\0\0\0c\0\0\0o', 'femenino', '79551771', 'Sopocachi', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(310, 'Pablo', '', 'Marmol', '', 'masculino', '65432102', 'Roca, Piedra', 'empleado_252437f6ec91780020210420_101025.png'),
(316, 'Pedro', '', 'Picapiedra', '', 'masculino', '65432101', 'Pierda dura', 'empleado_51dc84d2fb5bdfbb20210420_100345.jpg'),
(320, 'Pamela', '', 'Arias', 'Torrico', 'femenino', '77532010', '', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(321, 'Andrea', '', 'Rivero', 'Arias', 'femenino', '74140000', 'Achumani, calle 15', 'empleado_eb0224cb0ace656f20210420_095829.png'),
(325, 'Jhony', '', 'Fern&aacute;ndez', '', 'masculino', '78965410', '', 'empleado_eb0224cb0ace656f20210420_095829.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `primer_nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segundo_nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primer_apellido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segundo_apellido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rol` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `rol`, `usuario`, `clave`) VALUES
(19910907, 'Swania', 'Betty', 'Guarachi', 'Velasco', 'admin', 'Sguarachi', '$2y$10$gtoItQbzUsqkmyABxchti.5XYVUA27MlcY5IOztc8EEIFbeNe/o.6'),
(19910908, 'Raul', NULL, 'Gomez', 'Fernandez', 'invitado', 'Rgomez', '$2y$10$gtoItQbzUsqkmyABxchti.5XYVUA27MlcY5IOztc8EEIFbeNe/o.6');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19910910;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
